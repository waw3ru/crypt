/**
 * @license MIT
 * created by waweru
 */

import {randomBytes} from 'crypto';

export interface Credentials {
    hashPassword: string;
    authToken: string;
}
export const dataList: any[] = [
    'iam_a_peaceful_person_getting_through_life_peacefully',
    {
        name: 'James',
        gender: 'male',
        age: '21',
        bio: 'simple kid with nothing to hide. A nobody.'
    },
    901208823742213,
    'I_mix_numbers-with-1238197231_and_add-few-signs-$$%^',
    JSON.stringify({
        name: 'James',
        gender: 'male',
        age: '21',
        bio: 2891728193271846917,
        mean: '(*&^%$#@#$%^&*())'
    }),
    [
        {name: 'James', gender: 'male'},
        {name: 'Rachel', gender: 'female'},
        {name: 'Belle', gender: 'female'},
        {name: 'Don', gender: 'male'}
    ]
];
export const keys = [
    randomBytes(6).toString('hex'),
    randomBytes(4).toString('hex'),
    randomBytes(8).toString('hex'),
    randomBytes(10).toString('hex')
];
export const emitRandomIdx = (n: number) => (Math.floor(Math.random() * (n-1)+0));
