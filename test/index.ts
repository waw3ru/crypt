/**
 * Copyright (c) 2017 John Waweru Wambugu
 *
 * @license MIT
 * created by waweru
 */

import {deepStrictEqual, equal, notEqual} from 'assert';
import {randomBytes} from 'crypto';
import {isEqual} from 'lodash';
import {decrypt, encrypt} from '../index';
import {Credentials, dataList, emitRandomIdx, keys} from './data-samples';

describe('@tw/crypt testing suite', function() {

    context('Simple data encryption decryption functionality', () => {
        it('Should accept an user-chosen algorithm as an argument', (done: MochaDone) => {
            try {
                const data: Credentials = {
                    authToken: `thisismytoken123`,
                    hashPassword: `ahashedpasswordisthebest`
                };
                const key: string = randomBytes(8).toString('hex');
                const algorithm: string = `aes-256-ctr`;
                const encrypted: string = encrypt<Credentials>(data, key, algorithm);
                const decrypted: any = decrypt<Credentials>(encrypted, key, algorithm);
                deepStrictEqual({ a: data }, { a: decrypted });
            } catch(e) {
                equal(e, undefined || null);
            }
            done();
        });
        it('Should output segmented encrypted data', (done: MochaDone) => {
            const data: string = 'ineedmoremoney_up_in_here!';
            const key: string = randomBytes(8).toString('hex');
            try {
                const encrypted: string = encrypt<string>(data, key);
                const digestData = encrypted.split('|');
                equal(digestData.length, 3);
                const [dummyText, cipherIdx, cipher] = digestData;
                const digestCipher = cipher.split(';;');
                equal(digestCipher.length, 2);
            } catch(e) {
                equal(e, undefined || null);
            }
            done();
        });
    });

    context('Library adoption to diffrent data types using random keys', () => {
         it('Should encrypt-decrypt a random list of data', (done: MochaDone) => {
            try {
                const key: string = keys[emitRandomIdx(keys.length)];
                const result: boolean = dataList.reduce((acc: boolean, curr: any) => {
                    const encrypted: string = encrypt<any>(curr, key, null);
                    const decrypted: any = decrypt<any>(encrypted, key, null);
                    deepStrictEqual({ a: curr }, { a: (typeof curr === 'number') ? parseInt(decrypted) : decrypted });
                    return isEqual(curr, decrypted);
                }, 0);
            } catch(e) {
                equal(e, undefined || null);
            }
            done();
        });
    });

});
