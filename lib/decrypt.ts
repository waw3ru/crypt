/**
 * Copyright (c) 2017 John Waweru Wambugu
 *
 * @license MIT
 * created by waweru
 */

import {createDecipher, Decipher} from 'crypto';
import {cipherList} from './ciphers';

/**
 *
 * @param data
 * @param key
 * @param algorithm
 *
 * decrypt data and return original data structure
 */
export function decrypt<T>(data: string, key: string, algorithm: string): T;
export function decrypt<T>(data: string, key: string, algorithm?: string): T {

    let decipher: Decipher;

    if (!data) {
        return undefined;
    }
    // destructure data
    const [dummyText, idx, cipher] = data.split('|');
    const [encryptedData, salt] = cipher.split(';;');

    try {

        if (idx==='idx-n') {
            decipher = createDecipher(algorithm, `${key}||${salt}`);
        } else {
            decipher = createDecipher(cipherList[idx], `${key}||${salt}`);
        }

        let digest: string = decipher.update(encryptedData, 'hex', 'utf8');
        digest += decipher.final('utf8');
        return JSON.parse(digest);
    } catch (e) {
        throw e;
    }

}
