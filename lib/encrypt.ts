/**
 * Copyright (c) 2017 John Waweru Wambugu
 *
 * @license MIT
 * created by waweru
 */

import {Cipher, createCipher, randomBytes} from 'crypto';
import {random} from 'lodash';
import {cipherList} from './ciphers';

/**
 *
 * @param data
 * @param key
 * @param algorithm
 *
 * encrypt data using random algorithm
 */
export function encrypt<T>(data: T, key: string, algorithm?: string): string;
export function encrypt<T>(data: T | number, key: string, algorithm: string): string|undefined {
    let internalAlg: string;
    let idx: number;
    let cipher: Cipher;
    let digest: string;

    try {
        if (!data) {
            return undefined;
        }
        const salt: string = randomBytes(4).toString('hex');
        const dummyText: string = randomBytes(3).toString('hex');

        if (algorithm) {
            cipher = createCipher(algorithm, `${key}||${salt}`);
        } else {
            idx = Math.floor(random(cipherList.length-2));
            internalAlg = cipherList[idx];
            cipher = createCipher(internalAlg, `${key}||${salt}`);
        }

        const dataString: string = JSON.stringify( (typeof data === 'number') ? data.toString() : data );

        digest = cipher.update(dataString, 'utf8', 'hex');
        digest += cipher.final('hex');
        return `${dummyText}|${(!algorithm) ? idx : 'idx-n'}|${digest};;${salt}`;

    } catch(e) {
        throw e;
    }

}
