/**
 * Copyright (c) 2017 John Waweru Wambugu
 *
 * @license MIT
 * created by waweru
 */

export const cipherList: string[] = require('./.list.json').ciphers;
