# crypt

A simple encryption decryption library for node.js

> initial implementation was known as @slowday/encrypt this is a clone of the project

## How to install
1. Pre-requisites
    - node.js version >=5.x and npm version >=3.x must be installed
    You can get node.js [here](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwiE0YLDpoLVAhXHmpQKHR8fDj4QFghBMAQ&url=https%3A%2F%2Fnodejs.org%2Fen%2Fdownload%2F&usg=AFQjCNHZzjan4fCboPz6gh54VvJCUBVGEA)
    - Typescript compiler must be installed. Install via npm using `npm install -g typescript`
2. Guide on installation
- Clone the project 
- Run the following commands.
    * Install all project dependencies `npm install` or `yarn`
    * How to compile the project _(this compiles typescript)_ `npm run build` or `yarn run build` 
    * How to run tests _(this helps in seeing the library at work and also catch bugs)_ `npm test`
- After this you can study the project and start contributing your ideas

## Documentation
To avoid rebuilding all the time use `yarn run dev` or `npm run dev` to run typescript compiler on watch mode.

#### Encryption

Encryption functionality is found in the **lib/encrypt.ts** file.
The function definition is generic and is as shown below:-

```
function encrypt<T>(data: T|number, key: string, algorithm?: string): string;
```

The encryption function accepts `data` and `key` as required arguments where `data` can be either a number or a user-defined type. Generic function is useful for *TypeScript* users but as for *JavaScript* it's great to know the data type expected since there is no type checking for it.

If the algorithm is not provided the function will use a random algorithm to encrypt the data. The list of algorithm to be used is found in the file **lib/.list.json**. How the list of ciphers was generated please refer to **lib/_generate.ts**.
No merge request will be accepted for **.list.json** file though.
    

#### Decryption

Encryption functionality is found in the **lib/decrypt.ts** file.
The function definition is generic and is as shown below:-

```
function decrypt<T>(data: string, key: string, algorithm?: string): T;
```

The data-type for the generic function should match the one used when encrypting the data. This is necessary for *TypeScript* users for better debugging and intellisense for the IDE or code editor.

Algorithm argument should be provided if it was provided during encryption. This is not necessary since the function will detect whether it was provided during encryption or not; but for purposes code readability it is advised for you to follow that standard. The original data structure or type will be return on successful decryption.

All the functions named above run **_synchronously_** so you can just run the function on a `try ... catch` block.

## Usage and Example
- First of all run `npm install --save twcrypt` or `yarn add twcrypt` on your terminal

```
const {encrypt, decrypt} = require('twcrypt');
// for es6
// import { encrypt, decrypt } from 'twcrypt';

try {
    const data = {
        authToken: `thisismytoken123`,
        hashPassword: `ahashedpasswordisthebest`
    };
    const key: string = randomBytes(8).toString('hex');
    // const algorithm: string = `aes-256-ctr`;
    // const encrypted: string = encrypt(data, key, algorithm); // enforce specific algorithm
    const encrypted: string = encrypt(data, key);
    // const decrypted: any = decrypt(encrypted, key, algorithm); // if algorithm was included during encryption
    const decrypted: any = decrypt(encrypted, key);
    console.log('Encrypted data: ', encrypted);
    console.log('Decrypted data: ', decrypted); // check if they are the same
} catch(e) {
    console.log(e); // log errors that occur for debugging
}
```

> For usage in **TypeScript** please refer to the `/test` folder for even more enhaced usages.

## License
MIT
Copyright (c) 2017 John Waweru Wambugu